# typed: false
# frozen_string_literal: true

module MessagesHelper
  def new_message_count_print
    if new_message_count
      new_message_count.positive? ? "(#{new_message_count})" : ''
    end
  end
end
