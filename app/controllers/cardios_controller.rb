# typed: true
# frozen_string_literal: true

class CardiosController < ApplicationController
  extend T::Sig
  before_action :authenticate_user!
  before_action :set_prep
  before_action :user_owns_prep

  def index
    @cardios = @prep.cardios.order(created_at: :desc)
    @target_cardios = @prep.target_cardios.order(created_at: :desc)
    # gets minimum and maximum values for setting y axis chart values
    @minmax = (@cardios.map(&:duration) + @target_cardios.map(&:duration)).minmax
  end

  def new
    @cardio = Cardio.new
  end

  def create
    @cardio = @prep.cardios.new(cardio_params)
    if @cardio.save
      flash[:success] = 'Cardio entered!'
      redirect_to @prep
    else
      render :new
    end
  end

  class CardioParams < T::Struct
    const :duration, Integer
    const :activity, String
    const :style,    String
  end

  private

  def cardio_params
    TypedParams[CardioParams].new.extract!(params)
  end
end
