# typed: strict
# frozen_string_literal: true

# == Schema Information
#
# Table name: athletes
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Athlete < ApplicationRecord
  rolify
end
