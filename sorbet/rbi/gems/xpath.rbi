# This file is autogenerated. Do not edit it by hand. Regenerate it with:
#   srb rbi gems

# typed: true
#
# If you would like to make changes to this file, great! Please create the gem's shim here:
#
#   https://github.com/sorbet/sorbet-typed/new/master?filename=lib/xpath/all/xpath.rbi
#
# xpath-2.0.0

module XPath
  def self.generate; end
  extend XPath::DSL::TopLevel
  include XPath::DSL::TopLevel
end
module XPath::DSL
end
module XPath::DSL::TopLevel
  def anywhere(*expressions); end
  def attr(expression); end
  def axis(name, tag_name = nil); end
  def child(*expressions); end
  def contains(expression); end
  def css(selector); end
  def current; end
  def descendant(*expressions); end
  def name; end
  def next_sibling(*expressions); end
  def previous_sibling(*expressions); end
  def starts_with(expression); end
  def string; end
  def text; end
end
module XPath::DSL::ExpressionLevel
  def &(expression); end
  def +(*expressions); end
  def ==(expression); end
  def [](expression); end
  def and(expression); end
  def equals(expression); end
  def inverse; end
  def is(expression); end
  def n; end
  def normalize; end
  def one_of(*expressions); end
  def or(expression); end
  def string_literal; end
  def union(*expressions); end
  def where(expression); end
  def |(expression); end
  def ~; end
  include XPath::DSL::TopLevel
end
class XPath::Expression
  def arguments; end
  def arguments=(arg0); end
  def current; end
  def expression; end
  def expression=(arg0); end
  def initialize(expression, *arguments); end
  def to_s(type = nil); end
  def to_xpath(type = nil); end
  include XPath::DSL::ExpressionLevel
end
class XPath::Literal
  def initialize(value); end
  def value; end
end
class XPath::Union
  def arguments; end
  def each(&block); end
  def expression; end
  def expressions; end
  def initialize(*expressions); end
  def method_missing(*args); end
  def to_s(type = nil); end
  def to_xpath(type = nil); end
  include Enumerable
end
class XPath::Renderer
  def and(one, two); end
  def anywhere(element_names); end
  def attribute(current, name); end
  def axis(parent, name, tag_name); end
  def child(parent, element_names); end
  def contains(current, value); end
  def convert_argument(argument); end
  def css(current, selector); end
  def descendant(parent, element_names); end
  def equality(one, two); end
  def initialize(type); end
  def inverse(current); end
  def is(one, two); end
  def literal(node); end
  def next_sibling(current, element_names); end
  def node_name(current); end
  def normalized_space(current); end
  def one_of(current, values); end
  def or(one, two); end
  def previous_sibling(current, element_names); end
  def render(node); end
  def self.render(node, type); end
  def starts_with(current, value); end
  def string_function(current); end
  def string_literal(string); end
  def text(current); end
  def this_node; end
  def union(*expressions); end
  def variable(name); end
  def where(on, condition); end
end
module XPath::HTML
  def button(locator); end
  def checkbox(locator); end
  def definition_description(locator); end
  def field(locator); end
  def fieldset(locator); end
  def file_field(locator); end
  def fillable_field(locator); end
  def link(locator); end
  def link_or_button(locator); end
  def locate_field(xpath, locator); end
  def optgroup(locator); end
  def option(locator); end
  def radio_button(locator); end
  def select(locator); end
  def table(locator); end
  extend XPath::HTML
  include XPath::DSL::TopLevel
end
