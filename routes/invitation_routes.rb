# typed: strict
# frozen_string_literal: true

Rails.application.routes.draw do
  resources :invitations, only: [:show]

  get 'preps/:prep_id/coach_request/:id' => 'invitations#new', as: :new_coach_request
  post 'preps/:prep_id/coach_request/:id' => 'invitations#create', as: :coach_request
  get 'preps/:prep_id/invite/:id' => 'invitations#show', as: :invite_view
  post 'preps/:prep_id/invite/:id' => 'invitations#accept_invite', as: :accept_invite
  delete 'preps/:prep_id/invite/:id' => 'invitations#destroy', as: :deny_invite
end
