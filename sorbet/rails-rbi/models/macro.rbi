# This is an autogenerated file for dynamic methods in Macro
# Please rerun bundle exec rake rails_rbi:models[Macro] to regenerate.

# typed: strong
module Macro::ActiveRelation_WhereNot
  sig { params(opts: T.untyped, rest: T.untyped).returns(T.self_type) }
  def not(opts, *rest); end
end

module Macro::GeneratedAttributeMethods
  sig { returns(Integer) }
  def carbs; end

  sig { params(value: T.any(Numeric, ActiveSupport::Duration)).void }
  def carbs=(value); end

  sig { returns(T::Boolean) }
  def carbs?; end

  sig { returns(ActiveSupport::TimeWithZone) }
  def created_at; end

  sig { params(value: T.any(Date, Time, ActiveSupport::TimeWithZone)).void }
  def created_at=(value); end

  sig { returns(T::Boolean) }
  def created_at?; end

  sig { returns(Integer) }
  def fat; end

  sig { params(value: T.any(Numeric, ActiveSupport::Duration)).void }
  def fat=(value); end

  sig { returns(T::Boolean) }
  def fat?; end

  sig { returns(T.nilable(Integer)) }
  def fiber; end

  sig { params(value: T.nilable(T.any(Numeric, ActiveSupport::Duration))).void }
  def fiber=(value); end

  sig { returns(T::Boolean) }
  def fiber?; end

  sig { returns(Integer) }
  def id; end

  sig { params(value: T.any(Numeric, ActiveSupport::Duration)).void }
  def id=(value); end

  sig { returns(T::Boolean) }
  def id?; end

  sig { returns(T.nilable(Integer)) }
  def prep_id; end

  sig { params(value: T.nilable(T.any(Numeric, ActiveSupport::Duration))).void }
  def prep_id=(value); end

  sig { returns(T::Boolean) }
  def prep_id?; end

  sig { returns(Integer) }
  def protein; end

  sig { params(value: T.any(Numeric, ActiveSupport::Duration)).void }
  def protein=(value); end

  sig { returns(T::Boolean) }
  def protein?; end

  sig { returns(ActiveSupport::TimeWithZone) }
  def updated_at; end

  sig { params(value: T.any(Date, Time, ActiveSupport::TimeWithZone)).void }
  def updated_at=(value); end

  sig { returns(T::Boolean) }
  def updated_at?; end
end

module Macro::GeneratedAssociationMethods
  sig { returns(::Prep) }
  def prep; end

  sig { params(args: T.untyped, block: T.nilable(T.proc.params(object: ::Prep).void)).returns(::Prep) }
  def build_prep(*args, &block); end

  sig { params(args: T.untyped, block: T.nilable(T.proc.params(object: ::Prep).void)).returns(::Prep) }
  def create_prep(*args, &block); end

  sig { params(args: T.untyped, block: T.nilable(T.proc.params(object: ::Prep).void)).returns(::Prep) }
  def create_prep!(*args, &block); end

  sig { params(value: ::Prep).void }
  def prep=(value); end

  sig { returns(::Prep) }
  def reload_prep; end
end

module Macro::CustomFinderMethods
  sig { params(limit: Integer).returns(T::Array[Macro]) }
  def first_n(limit); end

  sig { params(limit: Integer).returns(T::Array[Macro]) }
  def last_n(limit); end

  sig { params(args: T::Array[T.any(Integer, String)]).returns(T::Array[Macro]) }
  def find_n(*args); end

  sig { params(id: Integer).returns(T.nilable(Macro)) }
  def find_by_id(id); end

  sig { params(id: Integer).returns(Macro) }
  def find_by_id!(id); end
end

class Macro < ApplicationRecord
  include Macro::GeneratedAttributeMethods
  include Macro::GeneratedAssociationMethods
  extend Macro::CustomFinderMethods
  extend Macro::QueryMethodsReturningRelation
  RelationType = T.type_alias { T.any(Macro::ActiveRecord_Relation, Macro::ActiveRecord_Associations_CollectionProxy, Macro::ActiveRecord_AssociationRelation) }
end

module Macro::QueryMethodsReturningRelation
  sig { returns(Macro::ActiveRecord_Relation) }
  def all; end

  sig { params(block: T.nilable(T.proc.void)).returns(Macro::ActiveRecord_Relation) }
  def unscoped(&block); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def select(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def reselect(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def order(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def reorder(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def group(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def limit(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def offset(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def joins(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def left_joins(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def left_outer_joins(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def where(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def rewhere(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def preload(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def extract_associated(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def eager_load(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def includes(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def from(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def lock(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def readonly(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def or(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def having(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def create_with(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def distinct(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def references(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def none(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def unscope(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def optimizer_hints(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def merge(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def except(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_Relation) }
  def only(*args); end

  sig { params(args: T.untyped, block: T.nilable(T.proc.void)).returns(Macro::ActiveRecord_Relation) }
  def extending(*args, &block); end

  sig do
    params(
      of: T.nilable(Integer),
      start: T.nilable(Integer),
      finish: T.nilable(Integer),
      load: T.nilable(T::Boolean),
      error_on_ignore: T.nilable(T::Boolean),
      block: T.nilable(T.proc.params(e: Macro::ActiveRecord_Relation).void)
    ).returns(ActiveRecord::Batches::BatchEnumerator)
  end
  def in_batches(of: 1000, start: nil, finish: nil, load: false, error_on_ignore: nil, &block); end
end

module Macro::QueryMethodsReturningAssociationRelation
  sig { returns(Macro::ActiveRecord_AssociationRelation) }
  def all; end

  sig { params(block: T.nilable(T.proc.void)).returns(Macro::ActiveRecord_Relation) }
  def unscoped(&block); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def select(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def reselect(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def order(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def reorder(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def group(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def limit(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def offset(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def joins(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def left_joins(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def left_outer_joins(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def where(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def rewhere(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def preload(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def extract_associated(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def eager_load(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def includes(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def from(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def lock(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def readonly(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def or(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def having(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def create_with(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def distinct(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def references(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def none(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def unscope(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def optimizer_hints(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def merge(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def except(*args); end

  sig { params(args: T.untyped).returns(Macro::ActiveRecord_AssociationRelation) }
  def only(*args); end

  sig { params(args: T.untyped, block: T.nilable(T.proc.void)).returns(Macro::ActiveRecord_AssociationRelation) }
  def extending(*args, &block); end

  sig do
    params(
      of: T.nilable(Integer),
      start: T.nilable(Integer),
      finish: T.nilable(Integer),
      load: T.nilable(T::Boolean),
      error_on_ignore: T.nilable(T::Boolean),
      block: T.nilable(T.proc.params(e: Macro::ActiveRecord_AssociationRelation).void)
    ).returns(ActiveRecord::Batches::BatchEnumerator)
  end
  def in_batches(of: 1000, start: nil, finish: nil, load: false, error_on_ignore: nil, &block); end
end

class Macro::ActiveRecord_Relation < ActiveRecord::Relation
  include Macro::ActiveRelation_WhereNot
  include Macro::CustomFinderMethods
  include Macro::QueryMethodsReturningRelation
  Elem = type_member(fixed: Macro)
end

class Macro::ActiveRecord_AssociationRelation < ActiveRecord::AssociationRelation
  include Macro::ActiveRelation_WhereNot
  include Macro::CustomFinderMethods
  include Macro::QueryMethodsReturningAssociationRelation
  Elem = type_member(fixed: Macro)
end

class Macro::ActiveRecord_Associations_CollectionProxy < ActiveRecord::Associations::CollectionProxy
  include Macro::CustomFinderMethods
  include Macro::QueryMethodsReturningAssociationRelation
  Elem = type_member(fixed: Macro)

  sig { params(records: T.any(Macro, T::Array[Macro])).returns(T.self_type) }
  def <<(*records); end

  sig { params(records: T.any(Macro, T::Array[Macro])).returns(T.self_type) }
  def append(*records); end

  sig { params(records: T.any(Macro, T::Array[Macro])).returns(T.self_type) }
  def push(*records); end

  sig { params(records: T.any(Macro, T::Array[Macro])).returns(T.self_type) }
  def concat(*records); end
end
