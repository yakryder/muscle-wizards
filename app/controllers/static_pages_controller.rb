# typed: true
# frozen_string_literal: true

class StaticPagesController < ApplicationController
  def home
    @no_nav = true
  end

  def about; end
end
