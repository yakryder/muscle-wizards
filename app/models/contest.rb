# typed: false
# frozen_string_literal: true

# == Schema Information
#
# Table name: contests
#
#  id         :integer          not null, primary key
#  title      :string
#  prep_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  date       :datetime
#  url        :string
#
class Contest < ApplicationRecord
  belongs_to :prep, touch: true
  validates :title, :date, presence: true
  validate :is_in_the_future
  validates :url, format: { with: %r{\Ahttps?://}, message: 'must start with http:// or https://' }, allow_blank: true
  # TODO: If :url doesn't have an http on it, slap it on there

  def is_in_the_future
    errors.add(:date, "can't be in the past") if date && date < Time.now
  end
end
