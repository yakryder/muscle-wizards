# Muscle Wizards

A contest prep app for bodybuilders and their coaches

## A note on Routes

We keep our routes in a routes directory under the project root, because it makes good sense and because it makes it easier to honor the Dominyin protocol.
