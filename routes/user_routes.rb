# typed: strict
# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }

  resources :users do
    resources :resources
    resources :philosophies, only: %i[new create update edit]
    resources :certifications, only: %i[new create update edit destroy]
  end

  get 'preps/:prep_id/coaches' => 'users#index', as: :coaches
  get 'preps/:prep_id/coach/:id' => 'users#show', as: :coach
  get 'preps/:prep_id/athlete/:id' => 'users#show', as: :athlete
end
