# typed: false
# frozen_string_literal: true

# == Schema Information
#
# Table name: certifications
#
#  id           :integer          not null, primary key
#  name         :string
#  date_granted :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :integer
#
class Certification < ApplicationRecord
  belongs_to :user
  validates :name, :date_granted, presence: true
  validate :is_in_the_future

  def is_in_the_future
    errors.add(:date_granted, "can't be a future date") if date_granted && date_granted > Time.now
  end
end
