# typed: strong
# This is an autogenerated file for Rails' mailers.
# Please rerun bundle exec rake rails_rbi:mailers to regenerate.
class InvitationMailer
  sig { params(recipient: T.untyped, prep: T.untyped).returns(ActionMailer::MessageDelivery) }
  def self.accept(recipient, prep); end

  sig { params(recipient: T.untyped, prep: T.untyped).returns(ActionMailer::MessageDelivery) }
  def self.invite(recipient, prep); end

  sig { params(recipient: T.untyped, prep: T.untyped).returns(ActionMailer::MessageDelivery) }
  def self.reject(recipient, prep); end
end
