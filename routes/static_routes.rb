# typed: strict
# frozen_string_literal: true

Rails.application.routes.draw do
  get 'home' => 'static_pages#home', as: :home
  get 'about' => 'static_pages#about', as: :about
end
