# typed: true
# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  gender                 :string
#  age                    :integer
#  bio                    :text
#  height                 :integer
#  phone_number           :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  coach                  :boolean
#
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :preps, dependent: :destroy
  has_attached_file :avatar, styles: { medium: '300X300#', thumb: '100x100#' }
  has_many :certifications, dependent: :destroy
  has_one :philosophy, dependent: :destroy
  has_many :invitations, dependent: :destroy
  has_many :resources, dependent: :destroy
  validates_attachment_content_type :avatar, content_type: %r{\Aimage/.*\Z}
  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 3.megabytes
  validates :name, :email, presence: true
  validates :age, :height, numericality: true, allow_blank: true
  validates :email, uniqueness: true
  validate :email_is_valid_format
  before_validation :downcase_email
  before_save :set_default_avatar

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.freeze

  def athleted_preps
    Prep.where(athlete: self)
  end

  def coached_preps
    Prep.where(coach: self)
  end

  private

  def email_is_valid_format
    errors.add(:email, 'Not a valid email address') unless email =~ VALID_EMAIL_REGEX
  end

  def downcase_email
    email&.downcase!
  end

  def set_default_avatar
    self.avatar = URI.parse('https://unsplash.it/400/?image=1061') if avatar.blank?
  end
end
