# typed: strict
# frozen_string_literal: true

# == Schema Information
#
# Table name: taggings
#
#  id         :integer          not null, primary key
#  photo_id   :integer
#  tag_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Tagging < ApplicationRecord
  belongs_to :photo
  belongs_to :tag
  validates_uniqueness_of :tag_id, scope: [:photo_id]
end
