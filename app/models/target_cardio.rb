# typed: strict
# frozen_string_literal: true

# == Schema Information
#
# Table name: target_cardios
#
#  id         :integer          not null, primary key
#  duration   :integer
#  activity   :string
#  style      :string
#  prep_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class TargetCardio < ApplicationRecord
  belongs_to :prep
  validates :duration, :activity, presence: true
  validates :duration, numericality: { greater_than: 0, less_than: 120, message: 'Value must be between 1 and 120' }
end
