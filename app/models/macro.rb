# typed: true
# frozen_string_literal: true

# == Schema Information
#
# Table name: macros
#
#  id         :integer          not null, primary key
#  protein    :integer
#  carbs      :integer
#  fat        :integer
#  fiber      :integer
#  prep_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Macro < ApplicationRecord
  belongs_to :prep, touch: true
  validates :protein, :carbs, :fat, presence: true
  validates :protein, :carbs, :fat,
            numericality: { greater_than: 0, less_than: 999, message: 'Value must be between 1 and 999' }
  validates :fiber, numericality: true, allow_blank: true

  def calories
    (protein + carbs) * 4 + fat * 9
  end
end
