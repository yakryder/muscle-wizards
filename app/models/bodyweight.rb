# typed: true
# frozen_string_literal: true

# == Schema Information
#
# Table name: bodyweights
#
#  id         :integer          not null, primary key
#  weight     :integer
#  prep_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Bodyweight < ApplicationRecord
  belongs_to :prep, touch: true
  validates :weight, presence: true
  validates :weight, numericality: { greater_than: 500, less_than: 5000, message: 'Weight must be between 50 and 500' }

  def display_weight
    weight / 10.0
  end
end
