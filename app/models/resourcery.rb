# typed: strict
# frozen_string_literal: true

# == Schema Information
#
# Table name: resourceries
#
#  id          :integer          not null, primary key
#  prep_id     :integer
#  resource_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  read        :boolean          default(FALSE)
#
class Resourcery < ApplicationRecord
  belongs_to :prep
  belongs_to :resource

  validates_uniqueness_of :resource_id, scope: :prep_id
end
