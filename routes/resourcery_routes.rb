# typed: strict
# frozen_string_literal: true

Rails.application.routes.draw do
  resources :resourceries, only: %i[new create destroy]
end
