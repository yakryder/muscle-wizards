# frozen_string_literal: true

source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.0'
# Use postgresql as the database for Active Record
gem 'pg', '~> 1.2.3'
# Use Puma as the app server
gem 'puma', '~> 5.1.1'
# Use SCSS for stylesheets
gem 'sassc-rails', '~> 2.1.2'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5.x'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.10.1'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
gem 'rake', '~> 13.0.1'

gem 'autoprefixer-rails', '~> 10.2.0'
gem 'aws-sdk'
gem 'cancancan'
gem 'chartkick'
gem 'devise', '~> 4.7.3'
gem 'faker', '~> 2.15.1'
gem 'figaro', '~> 1.2.0'
gem 'foundation-rails', '~> 6.6.2.0'
gem 'jquery-ui-rails'
gem 'kaminari'
gem 'magnific-popup-rails'
gem 'paperclip', '~> 6.1.0'
gem 'rails_12factor', group: :production
gem 'rolify'
gem 'simplecov', require: false, group: :test
gem 'simple_form', '~> 5.0.3'
gem 'sorbet-rails', '~> 0.7.3'
gem 'sorbet-runtime', '~> 0.5.6229'

group :development, :test do
  gem 'better_errors', '~> 2.9.1'
  gem 'binding_of_caller', '~> 1.0.0'
  gem 'capybara'
  gem 'database_cleaner'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '~> 3.0.5'
  gem 'sorbet', '~> 0.5.6229'
  gem 'web-console'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
