# typed: strict
# frozen_string_literal: true

Rails.application.routes.draw do
  resources :preps do
    collection do
      get :archived
    end
    resources :contests
    resources :photos
    resources :cardios
    resources :target_cardios
    resources :macros
    resources :target_macros
    resources :bodyweights
    resources :resources, only: [:show]
    resources :resourceries, only: [:index]
    resources :conversations, only: [:create] do
      resources :messages, only: %i[new create index]
    end
  end

  get 'preps/:prep_id/photos/poses/:tag' => 'photos#index', as: :pose
end
