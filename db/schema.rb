# typed: false
# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_201_212_201_220) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'active_storage_attachments', force: :cascade do |t|
    t.string 'name', null: false
    t.string 'record_type', null: false
    t.bigint 'record_id', null: false
    t.bigint 'blob_id', null: false
    t.datetime 'created_at', null: false
    t.index ['blob_id'], name: 'index_active_storage_attachments_on_blob_id'
    t.index %w[record_type record_id name blob_id], name: 'index_active_storage_attachments_uniqueness', unique: true
  end

  create_table 'active_storage_blobs', force: :cascade do |t|
    t.string 'key', null: false
    t.string 'filename', null: false
    t.string 'content_type'
    t.text 'metadata'
    t.string 'service_name', null: false
    t.bigint 'byte_size', null: false
    t.string 'checksum', null: false
    t.datetime 'created_at', null: false
    t.index ['key'], name: 'index_active_storage_blobs_on_key', unique: true
  end

  create_table 'active_storage_variant_records', force: :cascade do |t|
    t.bigint 'blob_id', null: false
    t.string 'variation_digest', null: false
    t.index %w[blob_id variation_digest], name: 'index_active_storage_variant_records_uniqueness', unique: true
  end

  create_table 'athletes', id: :serial, force: :cascade do |t|
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'athletes_roles', id: false, force: :cascade do |t|
    t.integer 'athlete_id'
    t.integer 'role_id'
    t.index %w[athlete_id role_id], name: 'index_athletes_roles_on_athlete_id_and_role_id'
  end

  create_table 'bodyweights', id: :serial, force: :cascade do |t|
    t.integer 'weight'
    t.integer 'prep_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['prep_id'], name: 'index_bodyweights_on_prep_id'
  end

  create_table 'cardios', id: :serial, force: :cascade do |t|
    t.integer 'duration'
    t.string 'activity'
    t.string 'style'
    t.integer 'prep_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['prep_id'], name: 'index_cardios_on_prep_id'
  end

  create_table 'certifications', id: :serial, force: :cascade do |t|
    t.string 'name'
    t.datetime 'date_granted'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.integer 'user_id'
    t.index ['user_id'], name: 'index_certifications_on_user_id'
  end

  create_table 'contests', id: :serial, force: :cascade do |t|
    t.string 'title'
    t.integer 'prep_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.datetime 'date'
    t.string 'url'
    t.index ['prep_id'], name: 'index_contests_on_prep_id'
  end

  create_table 'conversations', id: :serial, force: :cascade do |t|
    t.integer 'sender_id'
    t.integer 'recipient_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'invitations', id: :serial, force: :cascade do |t|
    t.integer 'prep_id'
    t.integer 'user_id'
    t.text 'message'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['prep_id'], name: 'index_invitations_on_prep_id'
    t.index ['user_id'], name: 'index_invitations_on_user_id'
  end

  create_table 'macros', id: :serial, force: :cascade do |t|
    t.integer 'protein'
    t.integer 'carbs'
    t.integer 'fat'
    t.integer 'fiber'
    t.integer 'prep_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['prep_id'], name: 'index_macros_on_prep_id'
  end

  create_table 'messages', id: :serial, force: :cascade do |t|
    t.text 'body'
    t.integer 'user_id'
    t.integer 'conversation_id'
    t.boolean 'read', default: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['conversation_id'], name: 'index_messages_on_conversation_id'
    t.index ['user_id'], name: 'index_messages_on_user_id'
  end

  create_table 'philosophies', id: :serial, force: :cascade do |t|
    t.text 'words'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.integer 'user_id'
    t.index ['user_id'], name: 'index_philosophies_on_user_id'
  end

  create_table 'photos', id: :serial, force: :cascade do |t|
    t.integer 'prep_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.string 'image_file_name'
    t.string 'image_content_type'
    t.integer 'image_file_size'
    t.datetime 'image_updated_at'
    t.index ['prep_id'], name: 'index_photos_on_prep_id'
  end

  create_table 'preps', id: :serial, force: :cascade do |t|
    t.string 'title'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.integer 'user_id'
    t.integer 'coach_id'
    t.boolean 'archived', default: false
    t.index ['user_id'], name: 'index_preps_on_user_id'
  end

  create_table 'resourceries', id: :serial, force: :cascade do |t|
    t.integer 'prep_id'
    t.integer 'resource_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.boolean 'read', default: false
    t.index ['prep_id'], name: 'index_resourceries_on_prep_id'
    t.index ['resource_id'], name: 'index_resourceries_on_resource_id'
  end

  create_table 'resources', id: :serial, force: :cascade do |t|
    t.integer 'user_id'
    t.text 'body'
    t.string 'title'
    t.string 'url'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.string 'upload_file_name'
    t.string 'upload_content_type'
    t.integer 'upload_file_size'
    t.datetime 'upload_updated_at'
    t.index ['user_id'], name: 'index_resources_on_user_id'
  end

  create_table 'roles', id: :serial, force: :cascade do |t|
    t.string 'name'
    t.string 'resource_type'
    t.integer 'resource_id'
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.index %w[name resource_type resource_id], name: 'index_roles_on_name_and_resource_type_and_resource_id'
    t.index ['name'], name: 'index_roles_on_name'
  end

  create_table 'taggings', id: :serial, force: :cascade do |t|
    t.integer 'photo_id'
    t.integer 'tag_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['photo_id'], name: 'index_taggings_on_photo_id'
    t.index ['tag_id'], name: 'index_taggings_on_tag_id'
  end

  create_table 'tags', id: :serial, force: :cascade do |t|
    t.string 'name'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'target_cardios', id: :serial, force: :cascade do |t|
    t.integer 'duration'
    t.string 'activity'
    t.string 'style'
    t.integer 'prep_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['prep_id'], name: 'index_target_cardios_on_prep_id'
  end

  create_table 'target_macros', id: :serial, force: :cascade do |t|
    t.integer 'protein'
    t.integer 'carbs'
    t.integer 'fat'
    t.integer 'fiber'
    t.integer 'prep_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['prep_id'], name: 'index_target_macros_on_prep_id'
  end

  create_table 'users', id: :serial, force: :cascade do |t|
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer 'sign_in_count', default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.inet 'current_sign_in_ip'
    t.inet 'last_sign_in_ip'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.string 'name'
    t.string 'gender'
    t.integer 'age'
    t.text 'bio'
    t.integer 'height'
    t.string 'phone_number'
    t.string 'avatar_file_name'
    t.string 'avatar_content_type'
    t.integer 'avatar_file_size'
    t.datetime 'avatar_updated_at'
    t.boolean 'coach'
    t.index ['email'], name: 'index_users_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_users_on_reset_password_token', unique: true
  end

  add_foreign_key 'active_storage_attachments', 'active_storage_blobs', column: 'blob_id'
  add_foreign_key 'active_storage_variant_records', 'active_storage_blobs', column: 'blob_id'
  add_foreign_key 'bodyweights', 'preps'
  add_foreign_key 'cardios', 'preps'
  add_foreign_key 'certifications', 'users'
  add_foreign_key 'contests', 'preps'
  add_foreign_key 'invitations', 'preps'
  add_foreign_key 'invitations', 'users'
  add_foreign_key 'macros', 'preps'
  add_foreign_key 'messages', 'conversations'
  add_foreign_key 'messages', 'users'
  add_foreign_key 'philosophies', 'users'
  add_foreign_key 'photos', 'preps'
  add_foreign_key 'preps', 'users'
  add_foreign_key 'resourceries', 'preps'
  add_foreign_key 'resourceries', 'resources'
  add_foreign_key 'resources', 'users'
  add_foreign_key 'taggings', 'photos'
  add_foreign_key 'taggings', 'tags'
  add_foreign_key 'target_cardios', 'preps'
  add_foreign_key 'target_macros', 'preps'
end
