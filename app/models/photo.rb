# typed: strict
# frozen_string_literal: true

# == Schema Information
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  prep_id            :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#
class Photo < ApplicationRecord
  belongs_to :prep, touch: true
  has_one :tag, through: :tagging
  has_one :tagging, dependent: :destroy
  has_attached_file :image, styles: { medium: '300x300>', thumb: '100x100#' }
  validates :image, attachment_presence: { message: 'You must attach a file' }
  validates_attachment_content_type :image, content_type: %r{\Aimage/.*\Z}
  validates_with AttachmentSizeValidator, attributes: :image, less_than: 3.megabytes
end
