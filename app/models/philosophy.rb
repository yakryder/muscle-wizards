# typed: strict
# frozen_string_literal: true

# == Schema Information
#
# Table name: philosophies
#
#  id         :integer          not null, primary key
#  words      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
class Philosophy < ApplicationRecord
  belongs_to :user
  validates_presence_of :words
  validates :words, length: { maximum: 2000 }
end
