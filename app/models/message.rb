# typed: true
# frozen_string_literal: true

# == Schema Information
#
# Table name: messages
#
#  id              :integer          not null, primary key
#  body            :text
#  user_id         :integer
#  conversation_id :integer
#  read            :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Message < ApplicationRecord
  belongs_to :user
  belongs_to :conversation

  validates :body, :user, :conversation, presence: true

  def message_time
    created_at.strftime('%m/%d/%y at %l:%M %p')
  end
end
