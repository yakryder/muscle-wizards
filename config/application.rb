# typed: true
# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MuscleWizards
  # The application instance
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Pulls routes from the given path
    # Allows us to separate routes out pre-Rails 6
    # Possibly beyond, as the Rails 6 way seems weird, whereas this is hacky but harmless
    def self.pull_routes_from(path)
      config.paths['config/routes.rb'] = Dir[Rails.root.join(path)]
    end

    pull_routes_from('routes/*.rb')
  end
end
