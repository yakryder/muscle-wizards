# typed: false
# frozen_string_literal: true

class CreateAthletes < ActiveRecord::Migration[5.0]
  def change
    create_table :athletes, &:timestamps
  end
end
