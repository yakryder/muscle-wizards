# typed: strict
# frozen_string_literal: true

Rails.application.routes.draw do
  get 'preps/:prep_id/self_coach' => 'preps#self_coach', as: :selfie
end
